# -*- coding: utf-8 -*-
"""
Created on Wed Mar 27 08:46:34 2019

@author: Simon
"""

from __future__ import print_function
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data as data_utils
import torch.optim as optim

import librosa

import pandas as pd
from sklearn.model_selection import train_test_split
import numpy as np
import matplotlib.pyplot as plt


class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()

        # 1. This network takes in a square (same width and height),
        # grayscale image as input
        # 2. It ends with a linear layer that represents the largest digit
        # input size 64 x 64
        # after the first conv layer, (W-F)/S + 1 = (64-5)/1 + 1 = 60
        # after one pool layer, this becomes (32, 30, 30)
        self.conv1 = nn.Conv2d(1, 20, 5, 1)

        # second conv layer: 32 inputs, 64 outputs , 5x5 conv
        # output size = (W-F)/S + 1 = (30-3)/1 + 1 = 28
        # output dimension: (64, 28, 28)
        # after another pool layer, this becomes (64, 14, 14)
        self.conv2 = nn.Conv2d(20, 64, 3, 1)

        self.conv2_drop = nn.Dropout2d()

        # 64 outputs * 3x3 filtered/pooled map  = 186624
        self.fc1 = nn.Linear(14*14*64, 250)
        self.fc2 = nn.Linear(250, 10)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = F.max_pool2d(x, 2, 2)
        x = F.relu(self.conv2(x))
        x = F.max_pool2d(x, 2)
        x = x.view(-1, 14*14*64)
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return F.log_softmax(x, dim=1)


def load_data():

    raw = pd.read_pickle('data/raw_dataset.pkl')
    raw1 = [[float(c) for c in samples] for samples in raw]
    labels = pd.read_pickle('data/labels.pkl')
    return raw1, labels


def create_tensors(raw, lbls):
    ###############################
    # Tensorize Data #
    ###############################
    raw = [x[:3500] if len(x) > 3500 else
           np.hstack([x, np.zeros(3500 - len(x))]) for x in raw]

#    print(max([len(x) for x in raw]))
#    print(min([len(x) for x in raw]))

    data = torch.tensor(raw)
    labels = torch.tensor([int(x) for x in lbls])

    return data, labels


def train(model, device, train_loader, log_interval, optimizer, epoch):
    model.train()
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        loss = F.nll_loss(output, target)
        loss.backward()
        optimizer.step()
        if batch_idx % log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.item()))


def test(model, device, loader):
    model.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in loader:
            data, target = data.to(device), target.to(device)
            output = model(data)
            # sum up batch loss
            test_loss += F.nll_loss(output, target, reduction='sum').item()
            # get the index of the max log-probability
            pred = output.argmax(dim=1, keepdim=True)
            correct += pred.eq(target.view_as(pred)).sum().item()

    test_loss /= len(loader.dataset)

    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{}'
          ' ({:.0f}%)\n'.format(test_loss, correct, len(loader.dataset),
                                100. * correct / len(loader.dataset)))

    return 100. * correct / len(loader.dataset)


def view_image(loader):
    dataiter = iter(loader)
    raw, labels = dataiter.next()

    rint = np.random.randint(len(raw))
    plt.figure(num=None, figsize=(10, 10), dpi=80)
    plt.rcParams.update({'font.size': 20})
    plt.plot(list(raw[rint]))
    plt.title('Original Instance\nTarget: {}'.format(labels[rint].item()))
    plt.xlabel('Samples (@ 8kHz)')
    plt.ylabel('Amplitude')
    plt.show()


if __name__ == '__main__':
    # Define device which will train NN (CPU probably)
    device = torch.device("cpu")
    ###############################
    # Set up variables #
    ###############################
    lr = 0.001
    epochs = 2
    log_interval = 2

    # Load Data
    raw, lbls = load_data()


    # Create test train split
    trni, tsti, trnl, tstl = train_test_split(raw, lbls, shuffle=True,
                                              test_size=0.1)

    # Create val train split
    trni, vali, trnl, vall = train_test_split(trni, trnl, shuffle=True,
                                              test_size=0.25)

    # Create datasets
    trn_i, trn_l = create_tensors(trni, trnl)
    val_i, val_l = create_tensors(vali, vall)
    tst_i, tst_l = create_tensors(tsti, tstl)

    ###############################
    # Create datasets #
    #############################
    trn = data_utils.TensorDataset(trn_i, trn_l)
    val = data_utils.TensorDataset(val_i, val_l)
    tst = data_utils.TensorDataset(tst_i, tst_l)

    ###############################
    # Create dataloaders #
    #############################
    train_loader = data_utils.DataLoader(trn, batch_size=20,
                                         shuffle=True)
    val_loader = data_utils.DataLoader(val, batch_size=20,
                                       shuffle=False)
    test_loader = data_utils.DataLoader(tst, batch_size=20,
                                        shuffle=False)
    view_image(train_loader)

    D_in = 4000
    H = 2**10
    D_out = 10

    model = Net().to(device)
    model = model.double()

    loss_fn = torch.nn.MSELoss()

    optimizer = torch.optim.Adam(model.parameters(), lr=lr)

    # Train model
    loss = []
    for epoch in range(1, epochs + 1):

        loss = train(model, device, train_loader, log_interval, optimizer,
                     epoch)

        final_acc = test(model, device, val_loader)
        print(final_acc)
