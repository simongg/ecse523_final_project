# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
import numpy as np
import matplotlib.pyplot as plt
import wave

from scipy.io import wavfile

from scipy import signal
import platform

if platform.uname()[1] == '156Nineron':
    spoken_digit_path = '../free-spoken-digit-dataset/recordings/'
if platform.uname()[1] == 'SGGL':
    spoken_digit_path = '/hdd/523/free-spoken-digit-dataset/recordings/'
if platform.uname()[1] == 'SGGT':
    spoken_digit_path = r'D:/free-spoken-digit-dataset/recordings/'

sample_rate, samples = wavfile.read(spoken_digit_path + '0_jackson_0.wav')

frequencies, times, spectrogram = signal.spectrogram(samples, sample_rate)

plt.figure(num=None, figsize=(18, 15), dpi=80)
plt.rcParams.update({'font.size': 30})
plt.pcolormesh(times, frequencies, spectrogram)
plt.imshow(spectrogram)
plt.ylabel('Frequency [Hz]')
plt.xlabel('Time [ms]')
plt.show()

plt.figure(num=None, figsize=(18, 10), dpi=80)
plt.rcParams.update({'font.size': 30})
plt.plot(samples)
plt.ylabel('Amplitude')
plt.xlabel('Time [ms]')
plt.show()
