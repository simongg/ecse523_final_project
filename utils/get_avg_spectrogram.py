#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr  1 10:50:21 2019

@author: simon
"""
import numpy as np
import matplotlib.image as mpimg
import platform
from os import listdir
from os.path import isfile, join
from matplotlib import pyplot as plt


if platform.uname()[1] == '156Nineron':
    spoken_digit_path = '../free-spoken-digit-dataset/'
    spectro_images = spoken_digit_path + 'spectrograms/'
    recordings = spoken_digit_path + 'recordings/'

if platform.uname()[1] == 'SGGL':
    spoken_digit_path = '/hdd/523/'
    spectro_images = spoken_digit_path + 'spectrograms/'
    recordings = spoken_digit_path + 'recordings/'

if platform.uname()[1] == 'SGGT':
    spoken_digit_path = r'D:/free-spoken-digit-dataset/'
    spectro_images = spoken_digit_path + 'spectrograms/noverlap=16/'
    recordings = spoken_digit_path + 'recordings/'

paths = [r'nfft128_large/nfft128_nover64_Large/',
         r'nfft512_large/nfft512_nover2_Large/']
for idx, path in enumerate(paths):
    spectro = spoken_digit_path + path
    print(path)
    for num in range(10):
        print(num)
        file_names = [f for f in listdir(spectro) if
                      isfile(join(spectro, f))
                      and '.png' and f[0] == str(num) in f]
        avg_spectro = np.zeros(np.shape((mpimg.imread(spectro +
                                                      file_names[0]))))
        for file in file_names:
            avg_spectro += (mpimg.imread(spectro + file))
        avg_spectro /= len(file_names)
        plt.figure(num=None, figsize=(15, 10), dpi=80)
        plt.rcParams.update({'font.size': 30})
        plt.imshow(avg_spectro[:, :, 1],  cmap='gray_r')
        plt.axis('off')

        plt.title('{}'.format(num))
        plt.savefig('{}avg_spectro_{}.pdf'.format((1+idx)*128, num))
