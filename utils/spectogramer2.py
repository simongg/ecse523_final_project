#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  5 11:17:04 2019

@author: simon
"""

from __future__ import division, print_function
from os import listdir
from os.path import isfile, join
import os

from matplotlib import pyplot as plt
import scipy.io.wavfile as wav
import numpy as np


def wav_to_spectrogram(audio_path, save_path, spectrogram_dimensions=(64, 64),
                       noverlap=16, cmap='gray_r', NFFT=128):
    """ Creates a spectrogram of a wav file.

    :param audio_path: path of wav file
    :param save_path:  path of spectrogram to save
    :param spectrogram_dimensions: number of pixels the spectrogram should be. Defaults (64,64)
    :param noverlap: See http://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.spectrogram.html
    :param cmap: the color scheme to use for the spectrogram. Defaults to 'gray_r'
    :return:
    """

    sample_rate, samples = wav.read(audio_path)
#    print(audio_path.split('/')[-1])
    fig = plt.figure()
    plt.figure(num=None, figsize=(18, 18), dpi=80)
    plt.rcParams.update({'font.size': 30})
#    fig.set_size_inches((spectrogram_dimensions[0]/fig.get_dpi(),
#                         spectrogram_dimensions[1]/fig.get_dpi()))
    ax = plt.Axes(fig, [0., 0., 1., 1.])
    ax.set_axis_off()
    fig.add_axes(ax)
    ax.specgram(samples, cmap=cmap, Fs=2, NFFT=NFFT, noverlap=noverlap)
    ax.xaxis.set_major_locator(plt.NullLocator())
    ax.yaxis.set_major_locator(plt.NullLocator())
#    ax.set_title(audio_path.split('/')[-1][0])
    fig.savefig(save_path, bbox_inches="tight", pad_inches=0)
    plt.close('all')


def dir_to_spectrogram(audio_dir, spectrogram_dir,
                       spectrogram_dimensions=(64, 64), NFFT=256, noverlap=128,
                       cmap='gray_r'):
    """ Creates spectrograms of all the audio files in a dir

    :param audio_dir: path of directory with audio files
    :param spectrogram_dir: path to save spectrograms
    :param spectrogram_dimensions: tuple specifying the dimensions in pixes of the created spectrogram. default:(64,64)
    :param noverlap: See http://docs.scipy.org/doc/scipy/reference/generated/scipy.signal.spectrogram.html
    :param cmap: the color scheme to use for the spectrogram. Defaults to 'gray_r'
    :return:
    """
    file_names = [f for f in listdir(audio_dir) if isfile(join(audio_dir, f))
                  and '.wav' in f]

    for file_name in file_names:
#        print(file_name)
        audio_path = audio_dir + file_name
        spectogram_path = spectrogram_dir + file_name.replace('.wav', '.png')
        wav_to_spectrogram(audio_path, spectogram_path,
                           spectrogram_dimensions=spectrogram_dimensions,
                           noverlap=noverlap, cmap=cmap, NFFT=NFFT)


if __name__ == '__main__':
    audio_dir = "../recordings/"
    spectrogram_dir = '../spectrograms/'
    NFFTs = np.array([16, 32, 64, 128, 256, 512, 1024])
    nos = np.array([8, 16, 32, 64, 128, 256, 512, 1024])
    for idx, nfft in enumerate(NFFTs):
        for no in nos[nos < nfft]:
            print(str(nfft) + ' ' + str(no))
            new_folder = 'nfft{}_nover{}/'.format(nfft, no)
            if not os.path.exists(spectrogram_dir + new_folder):
                os.mkdir(spectrogram_dir + new_folder)

            dir_to_spectrogram(audio_dir, spectrogram_dir+new_folder,
                               NFFT=nfft, noverlap=no)
