# -*- coding: utf-8 -*-
"""
Created on Sun Apr  7 15:46:11 2019

@author: sgeoff1
"""
from collections import defaultdict
import pickle
import matplotlib.pyplot as plt
import numpy as np


aep = [128, 512]
for epochs in aep:
    loss = pickle.load(open('/hdd/523/figures/loss_epochs{}.pkl'.format(epochs),
                            'rb'))
    acc = pickle.load(open('/hdd/523/figures/acc_epochs{}.pkl'.format(epochs),
                           'rb'))

    maximum_acc = defaultdict(float)
    max_lst = []
    max_key = []


    for key in acc.keys():
            maximum_acc[key] = max(acc[key])
            max_lst.append(max(acc[key]))
            max_key.append(key)

    keys = [max_key[np.argmin(max_lst)], max_key[np.argmax(max_lst)]]
    print(max_key[np.argmin(max_lst)])
    print(min(max_lst))
    print('')
    print(max_key[np.argmax(max_lst)])
    print(max(max_lst))

    plt.figure(num=None, figsize=(18, 15), dpi=80)
    plt.rcParams.update({'font.size': 30})

    for item in keys:
        plt.plot(loss[item], label='{} Loss'.format(item), linewidth=3)

    plt.title('Loss of CNN at every Epoch')
    plt.xlabel('Epoch')
    plt.ylabel('Negative Log-Likelyhood loss')
    plt.legend(fontsize=24)
    plt.grid()
    plt.savefig('loss_epochs{}_minmax.pdf'.format(epochs))

    plt.figure(num=None, figsize=(18, 15), dpi=80)
    plt.rcParams.update({'font.size': 30})

    for item in keys:
        plt.plot(acc[item], label='{} Accuracy'.format(item), linewidth=3)
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.title('Accuracy of CNN at every Epoch')
    plt.legend(fontsize=24)
    plt.grid()
    plt.savefig('acc_epochs{}_minmax.pdf'.format(epochs))


    keys = loss.keys()

    plt.figure(num=None, figsize=(18, 15), dpi=80)
    plt.rcParams.update({'font.size': 30})

    for item in keys:
        plt.plot(loss[item], label='{} Loss'.format(item), linewidth=3)

    plt.title('Loss of CNN at every Epoch')
    plt.xlabel('Epoch')
    plt.ylabel('Negative Log-Likelyhood loss')
    plt.legend(fontsize=24)
    plt.grid()
    plt.savefig('loss_epochs{}.pdf'.format(epochs))

    plt.figure(num=None, figsize=(18, 15), dpi=80)
    plt.rcParams.update({'font.size': 30})

    for item in keys:
        plt.plot(acc[item], label='{} Accuracy'.format(item), linewidth=3)
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.title('Accuracy of CNN at every Epoch')
    plt.legend(fontsize=24)
    plt.grid()
    plt.savefig('acc_epochs{}.pdf'.format(epochs))
