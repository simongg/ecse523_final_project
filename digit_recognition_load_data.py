# -*- coding: utf-8 -*-
"""
Spyder Editor

Author: Simon Geoffroy-Gagnon
"""
import numpy as np
import matplotlib.image as mpimg
import platform
import pickle
from os import listdir
from os.path import isfile, join
import scipy.io.wavfile as wav
import librosa

if platform.uname()[1] == '156Nineron':
    spoken_digit_path = '../free-spoken-digit-dataset/'
    spectro_images = spoken_digit_path + 'spectrograms/'
    recordings = spoken_digit_path + 'recordings/'

if platform.uname()[1] == 'SGGL':
    spoken_digit_path = '/hdd/523/free-spoken-digit-dataset/'
    spectro_images = spoken_digit_path + 'spectrograms/NFFT512/'
    recordings = spoken_digit_path + 'recordings/'

if platform.uname()[1] == 'SGGT':
    spoken_digit_path = r'D:/free-spoken-digit-dataset/'
    spectro_images = spoken_digit_path + 'spectrograms/'
    recordings = spoken_digit_path + 'recordings/'

raw_dataset = []
zp_raw_dataset = []
spectro_dataset = []
labels = []


def extract_mfcc(file_path, utterance_length):
    # Get raw .wav data and sampling rate from librosa's load function
    raw_w, sampling_rate = librosa.load(file_path, mono=True)

    # Obtain MFCC Features from raw data
    mfcc_features = librosa.feature.mfcc(raw_w, sampling_rate)
    if mfcc_features.shape[1] > utterance_length:
        mfcc_features = mfcc_features[:, 0:utterance_length]
    else:
        mfcc_features = np.pad(mfcc_features, ((0, 0), (0, utterance_length -
                                               mfcc_features.shape[1])),
                               mode='constant', constant_values=0)

    return mfcc_features

#
#raw_file_names = sorted([f for f in listdir(recordings)
#                         if isfile(join(recordings, f)) and '.wav' in f])

#mfcc = list()
#for file in raw_file_names:
#    mfcc.append(extract_mfcc(recordings + file, 4000))
#

if 1:
    raw_file_names = sorted([f for f in listdir(recordings)
                             if isfile(join(recordings, f)) and '.wav' in f])



    for file in raw_file_names:
#        sample_rate, samples = wav.read(recordings + file)
#        raw_dataset.append(samples)
        labels.append(file[0])
#
#    max_len = max([len(x) for x in raw_dataset])
#    for idx, sample in enumerate(raw_dataset):
#        zp_raw_dataset.append(np.hstack([sample, np.zeros(max_len - len(sample))]))

    spectro_file_names = sorted([f for f in listdir(spectro_images)
                                 if isfile(join(spectro_images, f)) and '.png'
                                 in f])

    for file in spectro_file_names:
        print(file)
        spectro_dataset.append(mpimg.imread(spectro_images + file))

    pickle_raw = 'data/raw_dataset.pkl'
    pickle_spectro = 'data/spectro_dataset_NFFT512.pkl'
    pickle_labels = 'data/labels.pkl'
    pickle_mfcc = 'data/mfcc_dataset'
#    with open(pickle_raw, 'wb') as f:
#        pickle.dump(raw_dataset, f)
#
    with open(pickle_spectro, 'wb') as f:
        pickle.dump(spectro_dataset, f)

#    with open(pickle_labels, 'wb') as f:
#        pickle.dump(labels, f)

#    with open(pickle_mfcc, 'wb') as f:
#        pickle.dump(mfcc, f)
