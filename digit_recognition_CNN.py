#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Mar 25 19:18:05 2019

@author: Simon Geoffroy-Gagnon
"""

from __future__ import print_function
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.utils.data as data_utils
import pandas as pd
from sklearn.model_selection import train_test_split
import numpy as np
import matplotlib.pyplot as plt
from collections import defaultdict
from os import listdir
from os.path import isfile, join
import matplotlib.image as mpimg
import pickle

class Net(nn.Module):
    def __init__(self):
        super(Net, self).__init__()

        # 1. This network takes in a square (same width and height),
        # grayscale image as input
        # 2. It ends with a linear layer that represents the largest digit
        # input size 64 x 64
        # after the first conv layer, (W-F)/S + 1 = (71-4)/1 + 1 = 68
        # 71-2+1 = 70
        # after one pool layer, this becomes (32, 35, 35)
        self.conv1 = nn.Conv2d(1, 32, 2, 1)

        self.conv1_drop = nn.Dropout2d()

        # second conv layer: 32 inputs, 64 outputs , 5x5 conv
        # output size = (W-F)/S + 1 = (35-2)/1 + 1 = 34
        # output dimension: (64, 32, 32)
        # after another pool layer, this becomes (64, 17, 17)
        self.conv2 = nn.Conv2d(32, 64, 2, 1)

        self.conv2_drop = nn.Dropout2d()

        # 64 outputs * 3x3 filtered/pooled map  = 186624
        self.fc1 = nn.Linear(17*17*64, 512)
        self.fc2 = nn.Linear(512, 10)

    def forward(self, x):
        x = F.relu(self.conv1(x))
        x = self.conv1_drop(x)
        x = F.max_pool2d(x, 2, 2)
        x = F.relu(self.conv2(x))
        x = self.conv2_drop(x)
        x = F.max_pool2d(x, 2)
        x = x.view(-1,  np.prod(x.shape[1:]))
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return F.log_softmax(x, dim=1)


class Net2(nn.Module):
    def __init__(self):
        super(Net2, self).__init__()

        # 1. This network takes in a square (same width and height),
        # grayscale image as input
        # 2. It ends with a linear layer that represents the largest digit
        # input size 64 x 64
        # after the first conv layer, (W-F)/S + 1 = (71-4)/1 + 1 = 68
        # 71-2+1 = 70
        # after one pool layer, this becomes (32, 35, 35)
        self.conv1 = nn.Conv2d(1, 32, 2, 1)

        self.conv1_drop = nn.Dropout2d()

        # second conv layer: 32 inputs, 64 outputs , 5x5 conv
        # output size = (W-F)/S + 1 = (35-2)/1 + 1 = 34
        # output dimension: (64, 32, 32)
        # after another pool layer, this becomes (64, 17, 17)
        self.conv2 = nn.Conv2d(32, 64, 2, 1)

        self.conv2_drop = nn.Dropout2d()

        # 64 outputs * 3x3 filtered/pooled map  = 186624
        self.fc1 = nn.Linear(17*17*64, 512)
        self.fc2 = nn.Linear(512, 10)

    def forward(self, x):
        x = F.relu(self.conv1(x))
#        x = self.conv1_drop(x)
        x = F.max_pool2d(x, 2, 2)
        x = F.relu(self.conv2(x))
#        x = self.conv2_drop(x)
        x = F.max_pool2d(x, 2)
        x = x.view(-1, np.prod(x.shape[1:]))
        x = F.relu(self.fc1(x))
        x = self.fc2(x)
        return F.log_softmax(x, dim=1)


def load_data(spectroD):
    images = pd.read_pickle('data/spectro_dataset.pkl')
    images = pd.read_pickle('data/spectro_dataset_{}.pkl'.format(spectroD))
    labels = pd.read_pickle('data/labels.pkl')
    return images, labels


def create_dataset_loader(img, lbls):
    ###############################
    # Tensorize Data #
    ###############################
    data = torch.tensor(img)
    labels = torch.tensor([int(x) for x in lbls])
    data = data.unsqueeze(1)

    return data, labels


def train(model, device, train_loader, log_interval, optimizer, epoch):
    model.train()
    losses = []
    for batch_idx, (data, target) in enumerate(train_loader):
        data, target = data.to(device), target.to(device)
        optimizer.zero_grad()
        output = model(data)
        loss = F.nll_loss(output, target)
        loss.backward()
        optimizer.step()
        losses.append(loss.item())
        if batch_idx % log_interval == 0:
            print('Train Epoch: {} [{}/{} ({:.0f}%)]\tLoss: {:.6f}'.format(
                epoch, batch_idx * len(data), len(train_loader.dataset),
                100. * batch_idx / len(train_loader), loss.item()))
    return np.mean(losses)


def test(model, device, loader):
    model.eval()
    test_loss = 0
    correct = 0
    with torch.no_grad():
        for data, target in loader:
            data, target = data.to(device), target.to(device)
            output = model(data)
            # sum up batch loss
            test_loss += F.nll_loss(output, target, reduction='sum').item()
            # get the index of the max log-probability
            pred = output.argmax(dim=1, keepdim=True)
            correct += pred.eq(target.view_as(pred)).sum().item()

    test_loss /= len(loader.dataset)

    print('\nTest set: Average loss: {:.4f}, Accuracy: {}/{}'
          ' ({:.0f}%)\n'.format(test_loss, correct, len(loader.dataset),
                                100. * correct / len(loader.dataset)))

    return 100. * correct / len(loader.dataset)


if __name__ == '__main__':
    # Define device which will train NN (CPU probably)
    device = torch.device("cpu")
    ###############################
    # Set up variables #
    ###############################
    # Set random seed
    seed = 1
    torch.manual_seed(seed)
    # Set learning rate
    lr = 0.0001
    m = 0.0001
    # Set no cuda
    # Set logging training status
    log_interval = 2
    # Set saving model?
    # Set Batch size
    batch_sz = 200
    val_batch_sz = 200
    # Set test batch size
    tst_batch_sz = 1000
    # Set number of epochs
    epochs = 64
    loss = defaultdict(list)
    acc = defaultdict(list)

    ##############################
    ##############################
    NFFTs = np.array([64, 1024])  # np.array([64, 128, 256, 512, 1024])
    nos = np.array([2, 32, 64, 512])  # np.array([16, 32, 64, 128, 256, 512])
    for idx, nfft in enumerate(NFFTs):
        for no in nos[nos < nfft]:
            spectroD = 'NFFT={}_noverlap={}'.format(nfft, no)

            print(spectroD)

            spectro_images = ('spectrograms'
                              '/nfft{}_nover{}/'.format(nfft, no))

            spectro_file_names = sorted([f for f in listdir(spectro_images)
                                         if isfile(join(spectro_images, f)) and
                                         '.png' in f])
            labels = []
            spectro_dataset = []
            for file in spectro_file_names:
                labels.append(file.split('/')[-1][0])
                spectro_dataset.append(mpimg.imread(spectro_images + file))

            # squeeze np array of image data
            spectro_dataset = np.array(spectro_dataset)[:, :, :, 0]

            # Create test train set
            trni, vali, trnl, vall = train_test_split(spectro_dataset, labels,
                                                      shuffle=True,
                                                      test_size=0.25)

            trn_i, trn_l = create_dataset_loader(trni, trnl)
            val_i, val_l = create_dataset_loader(vali, vall)

            ###############################
            # Create dataset #
            #############################
            trn = data_utils.TensorDataset(trn_i, trn_l)
            val = data_utils.TensorDataset(val_i, val_l)
        #    tst = data_utils.TensorDataset(tst_i, tst_l)

            ###############################
            # Create dataloaders #
            #############################
            train_loader = data_utils.DataLoader(trn, batch_size=batch_sz,
                                                 shuffle=True)
            val_loader = data_utils.DataLoader(val, batch_size=val_batch_sz,
                                               shuffle=True)
        #    test_loader = data_utils.DataLoader(tst, batch_size=tst_batch_sz,
        #                                        shuffle=True)

            # Set neural net (MNIST CNN)
            net = Net().to(device)

            # Define optimizer for this neural net
#            optimizer = torch.optim.SGD(net.parameters(), lr=lr, momentum=m)
            optimizer = torch.optim.Adamax(net.parameters(), lr=lr, momentum=m)

            # Train model
            for epoch in range(1, epochs + 1):
                curr_loss = train(net, device, train_loader, log_interval,
                                  optimizer, epoch)
                loss[spectroD].append(curr_loss)
                curr_acc = test(net, device, val_loader)
                acc[spectroD].append(curr_acc)

    plt.figure(num=None, figsize=(25, 20), dpi=80)
    plt.rcParams.update({'font.size': 34})

    for item in loss.keys():
        plt.plot(loss[item], label='{} Loss'.format(item), linewidth=3)

    plt.title('Loss of CNN at every Epoch')
    plt.xlabel('Epoch')
    plt.ylabel('Negative Log-Likelyhood loss')
    plt.legend(fontsize=20)
    plt.grid()
    plt.savefig('loss_epochs{}.pdf'.format(epochs))

    plt.figure(num=None, figsize=(25, 20), dpi=80)
    plt.rcParams.update({'font.size': 34})

    for item in acc.keys():
        plt.plot(acc[item], label='{} Accuracy'.format(item), linewidth=3)
    plt.ylabel('Accuracy')
    plt.xlabel('Epoch')
    plt.title('Accuracy of CNN at every Epoch')
    plt.legend(fontsize=20)
    plt.grid()
    plt.savefig('acc_epochs{}.pdf'.format(epochs))

    with open('loss_epochs{}.pkl'.format(epochs), 'wb') as f:
        pickle.dump(loss, f)

    with open('acc_epochs{}.pkl'.format(epochs), 'wb') as f:
        pickle.dump(acc, f)
